<?php

declare (strict_types=1);

namespace app\demo\controller;

use think\admin\Controller;
use think\admin\helper\QueryHelper;

class Category extends Controller
{
    public string $title='分类管理';

    /**
     * 分类管理
     * @auth true
     * @menu true
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index(): void
    {

        $this->_query('noob_category')->like('name')->layTable(function(){
            $this->title='语言分类管理';

        },function(QueryHelper $query){

        });

        $this->fetch();

    }

    /**
     * 添加分类
     * @auth true
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add(): void
    {
        $this->title = '添加分类';
        $this->_form('noob_category','form');
    }
    /**
     * 编辑分类
     * @auth true
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(): void
    {
        $this->title = '编辑分类';
        $this->_form('noob_category','form');
    }
    /**
     * 删除分类
     * @auth true
     * @return void
     * @throws \think\db\exception\DbException
     */
    public function remove(): void
    {

        $this->_delete('noob_category','id');
    }
    /**
     * 表单结果处理
     * @param boolean $state
     */
    protected function _form_result(bool $state)
    {
        if ($state) {
            $this->success('保存成功！', 'javascript:history.back()');
        }
    }


}