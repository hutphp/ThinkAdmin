<?php

declare (strict_types=1);

namespace app\demo\controller;

class Info extends \think\admin\Controller
{
    protected string $table='noob_info';
    /**
     * 管理信息
     * @auth true
     * @menu true
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index(): void
    {
        $this->_query($this->table)->alias('a')->equal('a.lang_id#lang_id')->like('a.title#title')->field('a.id,a.title,a.path,a.sort,a.lang_id,lang.name as lang_name')->leftJoin('noob_lang lang','a.lang_id=lang.id')->layTable(function(){
            $this->title='信息管理';
            $this->lang=$this->_query('noob_lang')->field('id,name')->order('sort desc,id asc')->db()->select()->toArray();
        });
    }
    /**
     * 添加信息
     * @auth true
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add()
    {
        $this->title = '添加信息';
        $this->lang=$this->_query('noob_lang')->field('id,name')->order('sort desc,id asc')->db()->select()->toArray();
        $this->_form($this->table,'form');
    }

    /**
     * 编辑信息
     * @auth true
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(): void
    {
        $this->title = '编辑信息';
        $this->lang=$this->_query('noob_lang')->field('id,name')->order('sort desc,id asc')->db()->select()->toArray();
        $this->_form($this->table,'form');
    }

    /**
     * 删除信息
     * @auth true
     * @return void
     * @throws \think\db\exception\DbException
     */
    public function remove(): void
    {

        $this->_delete($this->table,'id');
    }
    /**
     * 表单结果处理
     * @param boolean $state
     */
    protected function _form_result(bool $state)
    {
        if ($state) {
            $this->success('保存成功！', 'javascript:history.back()');
        }
    }
}