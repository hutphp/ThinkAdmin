<?php

declare (strict_types=1);

namespace app\demo\controller;

use think\admin\Controller;

class Template extends Controller
{
    public function edit()
    {
        $this->title="";
        $this->fetch('template/test')    ;
    }
}